package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.Type;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 03.10.2016.
 */
public class TypeDao {
    private ConnectionPool connectionPool;

    public TypeDao() { connectionPool = ConnectionPool.initConnection();}

    public List<Type> getType() throws SQLException {
        List<Type> types = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM projectmanagement.type");
        while(rs.next()) {
            types.add(new Type(rs.getInt(1),rs.getString(2)));
        }

        return types;
    }

 }
