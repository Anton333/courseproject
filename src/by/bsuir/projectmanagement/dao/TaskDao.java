package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 30.09.2016.
 */
public class TaskDao  {
    private ConnectionPool connectionPool;

    public TaskDao() { connectionPool = ConnectionPool.initConnection();}

    public void addTask(Type type,Risk risk,ProjectMember projectMember,String nameStatus,String namePriority,Task task,String projectName) throws SQLException {
        List<Type> types = new ArrayList<>();
        List<Risk> risks = new ArrayList<>();
        List<Role> roles = new ArrayList<>();
        List<ProjectMember> projectMembers = new ArrayList<>();
        int  idType = 0;
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT id,name FROM type\n" +
                "WHERE type.name=" + "'" + type.getName() + "'");
        while (rs.next()){
             types.add(new Type(rs.getInt(1),rs.getString(2)));
             idType =  rs.getInt(1);
        }

        Statement st1 = con.createStatement();
        ResultSet rs1 = st1.executeQuery("SELECT id FROM type");

        if(types.isEmpty() == true) {

            while(rs1.next()) {
                if(idType < rs1.getInt(1))
                    idType = rs1.getInt(1);
            }
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO type(id,name) VALUES(?,?)");
            idType++;
            preparedStatement.setInt(1, idType);
            preparedStatement.setString(2, type.getName());
            preparedStatement.execute();
            types.add(new Type(idType,type.getName()));
        }

        int idRole = 0;
        Statement st2 = con.createStatement();
        ResultSet rs2 = st2.executeQuery("SELECT id,role FROM role\n" +
                "WHERE role.role=" + "'" + projectMember.getRole() + "'");
        while (rs2.next()){
            roles.add(new Role(rs2.getInt(1),rs2.getString(2)));
            idRole =  rs2.getInt(1);
        }

        Statement st3 = con.createStatement();
        ResultSet rs3 = st3.executeQuery("SELECT * FROM role");

        if(roles.isEmpty() == true) {

            while(rs3.next()) {
                if(idRole < rs3.getInt(1))
                    idRole = rs3.getInt(1);
            }
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO role(id,role) VALUES(?,?)");
            idRole++;
            preparedStatement.setInt(1, idRole);
            preparedStatement.setString(2, projectMember.getRole());
            preparedStatement.execute();
            roles.add(new Role(idRole,projectMember.getRole()));
        }

        int idProjectMember = 0;
        Statement st4 = con.createStatement();
        ResultSet rs4 = st4.executeQuery("select project_member.id,name,surname,role_id\n" +
                        "from project_member inner join\n" +
                        "role on project_member.role_id = role.id\n" +
                        "where project_member.name = " + "'" + projectMember.getName() + "'" +
                        "and surname =" + "'" + projectMember.getSurname() + "'" +
                        "and role.role = " + "'" + projectMember.getRole() + "'");
        while (rs4.next()){
            projectMembers.add(new ProjectMember(rs4.getInt(1),rs4.getString(2),rs4.getString(3),projectMember.getRole()));
            idProjectMember =  rs4.getInt(1);
        }

        Statement st5 = con.createStatement();
        ResultSet rs5 = st5.executeQuery("SELECT * FROM project_member");

        if(projectMembers.isEmpty() == true) {

            while(rs5.next()) {
                if(idProjectMember < rs5.getInt(1))
                    idProjectMember = rs5.getInt(1);
            }
            PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO project_member(id,name,surname,role_id) VALUES(?,?,?,?)");
            idProjectMember++;
            preparedStatement.setInt(1, idProjectMember);
            preparedStatement.setString(2, projectMember.getName());
            preparedStatement.setString(3, projectMember.getSurname());
            preparedStatement.setInt(4, roles.get(0).getId());
            preparedStatement.execute();
            projectMembers.add(new ProjectMember(idProjectMember,projectMember.getName(),projectMember.getSurname(),roles.get(0).getRole()));
        }


        int idRisk = 0;

        Statement st8 = con.createStatement();
        ResultSet rs8 = st8.executeQuery("SELECT * FROM risk");
        while(rs8.next()) {
            if(idRisk < rs8.getInt(1))
                idRisk = rs8.getInt(1);
        }
        PreparedStatement preparedStatement = con.prepareStatement("INSERT INTO risk(id,description,price_damage,probability) VALUES(?,?,?,?)");
        idRisk++;
        preparedStatement.setInt(1, idRisk);
        preparedStatement.setString(2, risk.getDescription());
        preparedStatement.setString(3, risk.getPriceDamage());
        preparedStatement.setString(4, risk.getProbability());
        preparedStatement.execute();
        risks.add(new Risk(idRisk,risk.getDescription(),risk.getPriceDamage(),risk.getProbability()));

        int idTask = 0;

        Statement st9 = con.createStatement();
        ResultSet rs9 = st9.executeQuery("SELECT * FROM task");
        while(rs9.next()) {
            if(idTask < rs9.getInt(1))
                idTask = rs9.getInt(1);
        }

        StatusDao statusDao = new StatusDao();
        List<Status> statuses = statusDao.getStatusForTask(nameStatus);
        PriorityDao priorityDao = new PriorityDao();
        List<Priority> priorities = priorityDao.getPriorityForTask(namePriority);
        ProjectDao projectDao = new ProjectDao();
        List<Project> projects = projectDao.getProjectForTask(projectName);
        PreparedStatement preparedStatement1 = con.prepareStatement("INSERT INTO task VALUES(?,?,?,?,?,?,?,?,?)");
        idTask++;
        preparedStatement1.setInt(1, idTask);
        preparedStatement1.setString(2, task.getDateStart());
        preparedStatement1.setString(3, task.getDateEnd());
        preparedStatement1.setInt(4, projects.get(0).getId());
        preparedStatement1.setInt(5, projectMembers.get(0).getId());
        preparedStatement1.setInt(6, types.get(0).getId());
        preparedStatement1.setInt(7, priorities.get(0).getId());
        preparedStatement1.setInt(8, risks.get(0).getId());
        preparedStatement1.setInt(9, statuses.get(0).getId());
        preparedStatement1.execute();
    }

    public List<Task> getTasksForTheProject(String idProject) throws SQLException {
        List<Task> tasks = new ArrayList<>();

        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select * from task \n" +
                "where task.project_id =" + "'" + idProject + "'");
        while(rs.next()) {
            tasks.add(new Task(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getInt(9)));
        }
        return tasks;
    }
}
