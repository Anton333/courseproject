package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.Priority;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 27.09.2016.
 */
public class PriorityDao {
    private ConnectionPool connectionPool;

    public PriorityDao() { connectionPool = ConnectionPool.initConnection();}

    public List<String> getPrioroty() throws SQLException {
        List<String> priority = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT priority.name FROM priority");
        while(rs.next()) {
            priority.add(rs.getString(1));
        }



        connectionPool.freeConnection(con);

        return priority;

    }

    public List<Priority> getPriorityForTask(String name) throws SQLException {
        List<Priority> priorities = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT id,name FROM priority " +
                "WHERE priority.name =" + "'" + name + "'");
        while(rs.next()) {
            priorities.add(new Priority(rs.getInt(1),rs.getString(2)));
        }

        return priorities;
    }
}
