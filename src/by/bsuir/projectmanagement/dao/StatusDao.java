package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.Status;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 29.09.2016.
 */
public class StatusDao {
    private ConnectionPool connectionPool;

    public StatusDao() { connectionPool = ConnectionPool.initConnection();}

    public List<String> getStatus() throws SQLException {
        List<String> status = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT status.name FROM status");
        while(rs.next()) {
            status.add(rs.getString(1));
        }


        connectionPool.freeConnection(con);

        return status;
    }

    public List<Status> getStatusForTask(String name) throws SQLException {
        List<Status> statuses = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT id,name FROM status " +
                "WHERE status.name =" + "'" + name + "'");
        while(rs.next()) {
            statuses.add(new Status(rs.getInt(1),rs.getString(2)));
        }

        return statuses;
    }
}
