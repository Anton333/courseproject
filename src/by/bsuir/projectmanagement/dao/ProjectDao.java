package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 25.09.2016.
 */
public class ProjectDao {
    private ConnectionPool connectionPool;

    public ProjectDao() { connectionPool = ConnectionPool.initConnection();}

    public void addProject(String nameProject, String dateStart, String dateEnd) {
            try {
                Connection connection = connectionPool.getConnection();
                int idProject = 0;
                Statement st = connection.createStatement();
                ResultSet rs = st.executeQuery("SELECT * FROM Project");
                while(rs.next()) {
                    if(idProject < rs.getInt(1))
                        idProject = rs.getInt(1);
                }
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO project(id,name,date_start,date_end) VALUES(?,?,?,?)");
                idProject++;
                preparedStatement.setInt(1,idProject);
                preparedStatement.setString(2, nameProject);
                preparedStatement.setString(3,dateStart);
                preparedStatement.setString(4,dateEnd);
                preparedStatement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    public List<String> getStartDate(String nameProject) throws SQLException {
        List<String> dateStart = new ArrayList<>();

        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT project.date_start FROM project\n" +
                "where project.name =" + "'" + nameProject + "'");
        while(rs.next()) {
            dateStart.add(rs.getString(1));
        }

        ResultSet rs1 = st.executeQuery("SELECT task.date_end \n" +
                "from project inner join\n" +
                "task on project.id = task.project_id\n" +
                "where project.name =" + "'" + nameProject + "'");
        while (rs1.next()) {
            dateStart.add(rs1.getString(1));
        }
        connectionPool.freeConnection(con);

        return dateStart;

    }

    public List<Project> getProjectForTask(String name) throws SQLException {
        List<Project> projects = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select p.id,p.date_start,p.date_end,p.name,p.customer_id from project p " +
                        "left join\n" +
                "customer on p.customer_id = customer.id\n" +
                "where p.name =" + "'" + name + "'");
        while(rs.next()) {
            projects.add(new Project(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getInt(5)));
        }

        return projects;
    }

    public List<Project> getAllProjects() throws SQLException {
        List<Project> projects = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT id,date_start,date_end,name,customer_id FROM project ");
        while(rs.next()) {
            projects.add(new Project(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getInt(5)));
        }

        return projects;

    }

    }

