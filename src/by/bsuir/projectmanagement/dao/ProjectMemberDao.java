package by.bsuir.projectmanagement.dao;

import by.bsuir.projectmanagement.daoconnect.ConnectionPool;
import by.bsuir.projectmanagement.domain.ProjectMember;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toney on 01.10.2016.
 */
public class ProjectMemberDao {
    private ConnectionPool connectionPool;

    public ProjectMemberDao() { connectionPool = ConnectionPool.initConnection();}

    public List<ProjectMember> getProjectMember() throws SQLException {
        List<ProjectMember> projectMembers = new ArrayList<>();
        Connection con = connectionPool.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT project_member.name,project_member.surname, role.role FROM \n" +
                "project_member INNER JOIN role ON project_member.role_id = role.id");
        while(rs.next()) {
            projectMembers.add(new ProjectMember(rs.getString(1),rs.getString(2),rs.getString(3)));
        }

        return projectMembers;
    }
}
