package by.bsuir.projectmanagement.domain;

/**
 * Created by Toney on 25.09.2016.
 */
public class Project {
    private int id;
    private String dateStart;
    private String dateEnd;
    private String name;
    private int idCustomer;

    public Project(int id, String dateEnd, String dateStart, String name,int idCustomer) {
        this.id = id;
        this.dateEnd = dateEnd;
        this.dateStart = dateStart;
        this.idCustomer = idCustomer;
        this.name = name;
    }

    public Project() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
        this.idCustomer = idCustomer;
    }

    @Override
    public String toString() {
        return "Project{" +
                "dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", name='" + name + '\'' +
                ", idCustomer=" + idCustomer +
                '}';
    }
}
