package by.bsuir.projectmanagement.domain;

/**
 * Created by Toney on 25.09.2016.
 */
public class Risk {
    private int id;
    private String description;
    private String priceDamage;
    private String probability;

    public Risk(int id, String description, String priceDamage, String probability) {
        this.id = id;
        this.description = description;
        this.priceDamage = priceDamage;
        this.probability = probability;
    }

    public Risk() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriceDamage() {
        return priceDamage;
    }

    public void setPriceDamage(String priceDamage) {
        this.priceDamage = priceDamage;
    }

    public String getProbability() {
        return probability;
    }

    public void setProbability(String probability) {
        this.probability = probability;
    }

    @Override
    public String toString() {
        return "Risk{" +
                "description='" + description + '\'' +
                ", priceDamage=" + priceDamage +
                ", probability=" + probability +
                '}';
    }


}
