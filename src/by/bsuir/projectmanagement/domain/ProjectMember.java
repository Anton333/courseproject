package by.bsuir.projectmanagement.domain;

/**
 * Created by Toney on 25.09.2016.
 */
public class ProjectMember {
    private int id;
    private String name;
    private String surname;
    private String role;

    public ProjectMember(String name, String surname, String role) {
        this.name = name;
        this.surname = surname;
        this.role = role;
    }

    public ProjectMember(int id, String role, String surname, String name) {
        this.id = id;
        this.role = role;
        this.surname = surname;
        this.name = name;
    }

    public ProjectMember() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return name + " " + surname + " " + role;
    }
}
