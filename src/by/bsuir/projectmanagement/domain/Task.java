package by.bsuir.projectmanagement.domain;

/**
 * Created by Toney on 25.09.2016.
 */
public class Task {
    private String name;
    private String dateStart;
    private String dateEnd;
    private int idProject;
    private int idProjectMember;
    private int idType;
    private int idPriority;
    private int idRisk;
    private int idStatus;

    public Task() {
    }

    public Task(String name, String dateStart, String dateEnd, int idProject, int idProjectMember, int idType, int idPriority, int idRisk, int idStatus) {
        this.name = name;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.idProject = idProject;
        this.idProjectMember = idProjectMember;
        this.idType = idType;
        this.idPriority = idPriority;
        this.idRisk = idRisk;
        this.idStatus = idStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public int getIdProject() {
        return idProject;
    }

    public void setIdProject(int idProject) {
        this.idProject = idProject;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getIdProjectMember() {
        return idProjectMember;
    }

    public void setIdProjectMember(int idProjectMember) {
        this.idProjectMember = idProjectMember;
    }

    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    public int getIdPriority() {
        return idPriority;
    }

    public void setIdPriority(int idPriority) {
        this.idPriority = idPriority;
    }

    public int getIdRisk() {
        return idRisk;
    }

    public void setIdRisk(int idRisk) {
        this.idRisk = idRisk;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    @Override
    public String toString() {
        return "Task1{" +
                "name='" + name + '\'' +
                ", dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", idProject=" + idProject +
                ", idProjectMember=" + idProjectMember +
                ", idType=" + idType +
                ", idPriority=" + idPriority +
                ", idRisk=" + idRisk +
                ", idStatus=" + idStatus +
                '}';
    }
}
