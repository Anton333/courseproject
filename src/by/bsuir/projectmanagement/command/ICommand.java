package by.bsuir.projectmanagement.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 25.09.2016.
 */
public interface ICommand {
    public String execute(HttpServletRequest request,
                   HttpServletResponse response)
         throws ServletException,IOException,ClassNotFoundException,SQLException;
}
