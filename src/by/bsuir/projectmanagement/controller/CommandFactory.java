package by.bsuir.projectmanagement.controller;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.logic.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toney on 25.09.2016.
 */
public class CommandFactory {
    private static final String COMMAND_PARAMETER = "command";
    private  static CommandFactory instance = null;

    Map<String, ICommand> commands = new HashMap<>();

    private CommandFactory() {
         commands.put("Create Project", new CreateProject());
         commands.put("Create Task", new CreateTask());
         commands.put("Choice Project Member", new ChoiceProjectMember());
         commands.put("Project Member", new ProjectMemeber());
         commands.put("Choice Type", new ChoiceType());
         commands.put("Type Task", new Task1());
         commands.put("Enough", new ReturnToStartPage());
         commands.put("More Task", new AddTask());
         commands.put("Choice Project" ,new ChoiceProject());
         commands.put("Show Task", new ShowTasks());
    }

    public ICommand getCommand(HttpServletRequest request) {
        String action = request.getParameter(COMMAND_PARAMETER);
        ICommand command = commands.get(action);
        return command;
    }

    public static CommandFactory getInstance() {
        if(instance == null) {
            instance = new CommandFactory();
        }
        return instance;
    }
}
