package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.PriorityDao;
import by.bsuir.projectmanagement.dao.ProjectDao;
import by.bsuir.projectmanagement.dao.StatusDao;
import by.bsuir.projectmanagement.domain.ProjectMember;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 01.10.2016.
 */
public class ProjectMemeber implements ICommand{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
        HttpSession session = request.getSession();
        ProjectMember projectMember = new ProjectMember();
        String [] informationAboutProjectMember = request.getParameter("projectMember").split(" ");
        projectMember.setName(informationAboutProjectMember[0]);
        projectMember.setSurname(informationAboutProjectMember[1]);
        projectMember.setRole(informationAboutProjectMember[2]);
        session.setAttribute("nameProjectMember", projectMember.getName());
        session.setAttribute("surnameProjectMember",projectMember.getSurname());
        session.setAttribute("roleProjectMember",projectMember.getRole());
        ProjectDao projectDao = new ProjectDao();
        PriorityDao priorityDao = new PriorityDao();
        StatusDao statusDao = new StatusDao();
        session.setAttribute("listOfStartDate", projectDao.getStartDate((String) session.getAttribute("nameProject")));
        session.setAttribute("listOfPriorities",priorityDao.getPrioroty());
        request.setAttribute("listOfStatus", statusDao.getStatus());
        return "/addTask.jsp";
    }
}
