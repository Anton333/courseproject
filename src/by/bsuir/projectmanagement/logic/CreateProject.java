package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.PriorityDao;
import by.bsuir.projectmanagement.dao.ProjectDao;
import by.bsuir.projectmanagement.dao.StatusDao;
import by.bsuir.projectmanagement.domain.Project;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 25.09.2016.
 */
public class CreateProject implements ICommand{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
        Project project = new Project();
        project.setName(request.getParameter("nameProject"));
        project.setDateStart(request.getParameter("dateStart"));
        project.setDateEnd(request.getParameter("dateEnd"));
        ProjectDao projectDao = new ProjectDao();
        PriorityDao priorityDao = new PriorityDao();
        StatusDao statusDao = new StatusDao();
        HttpSession session = request.getSession();
        session.setAttribute("nameProject",project.getName());
        request.setAttribute("nameProject",project.getName());
        projectDao.addProject(project.getName(), project.getDateStart(), project.getDateEnd());
        session.setAttribute("listOfStartDate", projectDao.getStartDate(request.getParameter("nameProject")));
        session.setAttribute("listOfPriorities",priorityDao.getPrioroty());
        session.setAttribute("listOfStatus", statusDao.getStatus());
        return "/addTask.jsp";
    }
}
