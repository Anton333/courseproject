package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 08.10.2016.
 */
public class AddTask implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
        return "/addTask.jsp";
    }
}
