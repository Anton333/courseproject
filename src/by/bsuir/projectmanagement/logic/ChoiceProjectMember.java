package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.ProjectMemberDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 30.09.2016.
 */
public class ChoiceProjectMember implements ICommand {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {

        ProjectMemberDao projectMemberDao = new ProjectMemberDao();
        request.setAttribute("listOfProjectMember" , projectMemberDao.getProjectMember());

        return "/choiceProjectMember.jsp";
    }
}
