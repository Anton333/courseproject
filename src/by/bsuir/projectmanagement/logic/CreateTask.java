package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.TaskDao;
import by.bsuir.projectmanagement.domain.*;
import by.bsuir.projectmanagement.domain.Task;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
/**
 * Created by Toney on 27.09.2016.
 */
public class CreateTask implements ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
        HttpSession session = request.getSession();
        by.bsuir.projectmanagement.domain.Task task = new Task();
        Risk risk = new Risk();
        Type type = new Type();
        ProjectMember projectMember = new ProjectMember();
        Role role = new Role();
        Status status = new Status();
        Priority priority = new Priority();
        String nameProject = (String) session.getAttribute("nameProject");
        task.setName(request.getParameter("nameTask"));
        task.setDateEnd(request.getParameter("dateEndTask"));
        task.setDateStart(request.getParameter("dateStart"));
        risk.setDescription(request.getParameter("descriptionRisk"));
        risk.setPriceDamage(request.getParameter("priceDamage"));
        risk.setProbability(request.getParameter("probabilityRisk"));
        priority.setName(request.getParameter("priority"));
        status.setName(request.getParameter("status"));
        type.setName(request.getParameter("typeTask"));
        projectMember.setName(request.getParameter("nameProjectMember"));
        projectMember.setSurname( request.getParameter("surnameProjectMember"));
        projectMember.setRole( request.getParameter("roleProjectMember"));
        //request.setAttribute("nameProject",request.getParameter("nameProject"));

        TaskDao taskDao = new TaskDao();
        taskDao.addTask(type,risk,projectMember,status.getName(),priority.getName(),task,nameProject);
        return "/choiceActionByProject.jsp";
    }
}
