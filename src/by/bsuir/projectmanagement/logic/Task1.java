package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.PriorityDao;
import by.bsuir.projectmanagement.dao.ProjectDao;
import by.bsuir.projectmanagement.dao.StatusDao;
import by.bsuir.projectmanagement.dao.TypeDao;
import by.bsuir.projectmanagement.domain.ProjectMember;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 03.10.2016.
 */
public class Task1 implements ICommand{

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {

        HttpSession session = request.getSession();
        ProjectMember projectMember = new ProjectMember();
       // String [] informationAboutProjectMember = (String[]) session.getAttribute("nameProjectMember");
        projectMember.setName((String) session.getAttribute("nameProjectMember"));
        //projectMember.setSurname(informationAboutProjectMember[1]);
       // projectMember.setRole(informationAboutProjectMember[2]);
        ProjectDao projectDao = new ProjectDao();
        PriorityDao priorityDao = new PriorityDao();
        StatusDao statusDao = new StatusDao();
        TypeDao typeDao = new TypeDao();
        session.setAttribute("listOfStartDate", projectDao.getStartDate((String) session.getAttribute("nameProject")));
        session.setAttribute("listOfPriorities",priorityDao.getPrioroty());
        session.setAttribute("listOfStatus", statusDao.getStatus());
        session.setAttribute("typeTask",request.getParameter("typeTask") );
      //  request.setAttribute("nameProjectMember",projectMember.getName());
        return "/addTask.jsp";
    }
}
