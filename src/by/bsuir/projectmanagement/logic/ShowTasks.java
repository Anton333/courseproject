package by.bsuir.projectmanagement.logic;

import by.bsuir.projectmanagement.command.ICommand;
import by.bsuir.projectmanagement.dao.TaskDao;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Toney on 11.10.2016.
 */
public class ShowTasks implements ICommand {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ClassNotFoundException, SQLException {
        HttpSession session = request.getSession();
      //  session.getAttribute("projectId");
        TaskDao taskDao = new TaskDao();
        request.setAttribute("taskOfList", taskDao.getTasksForTheProject(request.getParameter("projectId")));
        return "showTask.jsp";
    }
}
