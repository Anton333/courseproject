<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Toney
  Date: 25.09.2016
  Time: 22:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
     <c:out value="${sessionScope.nameProject}"/>
     <form method="post" action="/Controller">
     <input type="text" name="nameTask" pattern="[A-Z][a-z]{1,20}" placeholder="Введите название задачи" required>
         <select  name="dateStart">
         <c:forEach var="dateStart" items="${listOfStartDate}">
             <option>${dateStart}</option>
         </c:forEach>
             </select>
         <input type="text" name="dateEndTask" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" placeholder="Введите дату окончания задачи" required>


         <input type="text" name="descriptionRisk" pattern="[A-Z][a-z]{1,100}" placeholder="Описание риска" required>
         <input type="text" name="priceDamage" pattern="^[1-9][0-9]" placeholder="Введите цену ущерба" required>
         <input type="text" name="probabilityRisk" pattern="^(100|[1-9]?\d)$" placeholder="Введите вероятность" required>
         <select  name="priority">
         <c:forEach var="priority" items="${listOfPriorities}">
             <option>${priority}</option>
         </c:forEach>
             </select>
         <input type="text" name="nameProjectMember" value="${sessionScope.nameProjectMember}" pattern="[A-Z][a-z]{1,100}" placeholder="Имя участника проекта" required>
         <input type="text" name="surnameProjectMember" value="${sessionScope.surnameProjectMember}" pattern="[A-Z][a-z]{1,100}" placeholder="Фамилия участника проекта" required>
         <input type="text" name="roleProjectMember" value="${sessionScope.roleProjectMember}" pattern="[A-Z][a-z]{1,100}" placeholder="Роль участника проекта " required>
         <input type="text" name="typeTask" value="${sessionScope.typeTask}" pattern="[A-Z][a-z]{1,100}" placeholder="Тип задачи " required>
         <select  name="status">
         <c:forEach var="status" items="${listOfStatus}">
             <option>${status}</option>
         </c:forEach>
             </select>
         <input type = "submit" name = "command" value="Create Task">

     </form>
     <form method="post" action="/Controller">
     <input type = "submit" name = "command" value="Choice Project Member" >
         </form>
     <form method="post" action="/Controller">
         <input type = "submit" name = "command" value="Choice Type" >
     </form>
</body>
</html>
