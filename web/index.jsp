<%--
  Created by IntelliJ IDEA.
  User: Toney
  Date: 24.09.2016
  Time: 18:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title></title>
    <p>Hello World!</p>
  </head>
  <body>
  <form method="post" action="/Controller">
    <input type="text" name="nameProject" pattern="[A-Z][a-z]{1,20}" placeholder="Введите название проекта" required>
    <input type="text" name="dateStart" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" placeholder="Введите дату начала проекта" required>
    <input type="text" name="dateEnd" pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])" placeholder="Введите дату окончания проекта" required>
    <div class="commands">
      <input type = "submit" name = "command" value="Create Project">
    </div>
  </form>
  <form method="get" action="/Controller">
    <input type="submit" name="command" value="Choice Project">
  </form>
  </body>
</html>
