<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Toney
  Date: 11.10.2016
  Time: 23:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<form method="get" action="/Controller">
<c:set var="taskOfList" value="${taskOfList}"/>
<c:if test="${not empty taskOfList}">
    <c:forEach var="task" items="${taskOfList}">
        <ul>
            <li>${task.id}</li>
        </ul>
    </c:forEach>
</c:if>
<c:if test="${empty taskOfList}">
    <h1>empty</h1>
</c:if>
    </form>
</body>
</html>
