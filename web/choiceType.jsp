<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Toney
  Date: 03.10.2016
  Time: 23:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>

<form method="post" action="/Controller">
  <table>
  <select  name="typeTask">
    <c:forEach var="typeTask" items="${listOfTypes}">
      <option>${typeTask}</option>
    </c:forEach>
  </select>
  <input type = "submit" name = "command" value="Type Task">
    </table>
</form>
</body>
</html>
